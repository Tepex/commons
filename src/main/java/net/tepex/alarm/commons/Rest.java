package net.tepex.alarm.commons;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.lang.reflect.Modifier;

import retrofit.ErrorHandler;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

/**
 * Клиент по умолчанию. При надобности - реализуется свой.
 */
public class Rest
{
	public static synchronized RestApi getApi(String baseUrl)
	{
		if(api == null)
		{
			RestAdapter adapter = getRestBuilder(baseUrl).build();
			api = adapter.create(RestApi.class);
		}
		return api;
	}
	
	public static Gson getGson()
	{
		return gson;
	}
	
	public static synchronized RestAdapter.Builder getRestBuilder(String baseUrl)
	{
		if(restBuilder == null)
		{
			restBuilder = new RestAdapter.Builder()
				.setLogLevel(RestAdapter.LogLevel.FULL)
				.setEndpoint(baseUrl)
				.setConverter(new GsonConverter(gson))
				.setErrorHandler(new AlarmErrorHandler());
		}
		return restBuilder;
	}
	
	private static RestApi api;
	private static RestAdapter.Builder restBuilder;
	
	public static final String DF = "dd.MM.yyyy HH:mm:ss";
	
	private static Gson gson = new GsonBuilder()
		.excludeFieldsWithModifiers(Modifier.STATIC, Modifier.TRANSIENT)
		.setDateFormat(DF)
		.create();	
	
	public static final String LOCAL = "http://localhost:8080/alarm";
	public static final String MAIN = "http://tepex.net:8080/alarm";
}