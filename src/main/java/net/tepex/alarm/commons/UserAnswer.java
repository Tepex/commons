package net.tepex.alarm.commons;

import net.tepex.alarm.commons.resources.Group;
import net.tepex.alarm.commons.resources.User;

/**
 * Ответ сервера на авторизацию и регистрацию.
 */
public class UserAnswer
{
	public UserAnswer(User user, Group[] groups)
	{
		this.user = user;
		this.groups = groups;
	}
	
	public User getUser()
	{
		return user;
	}
	
	public Group[] getGroups()
	{
		return groups;
	}
	
	private User user;
	private Group[] groups;
}