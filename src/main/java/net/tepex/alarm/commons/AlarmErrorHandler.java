package net.tepex.alarm.commons;

import retrofit.ErrorHandler;
import retrofit.RetrofitError;

import retrofit.client.Response;

/**
 * Реализация интерфейса ErrorHandler для библиотеки Retrofit.
 * Выкидывание исключения RestException в случае если HTTP-код возврата будет 400, 404 или 500.
 */
class AlarmErrorHandler implements ErrorHandler
{
	@Override
	public Throwable handleError(RetrofitError cause)
	{
		Response r = cause.getResponse();
		if(r != null && (r.getStatus() == 500 || r.getStatus() == 400 || r.getStatus() == 404)) return new RestException(cause);
		return cause;
	}
}
