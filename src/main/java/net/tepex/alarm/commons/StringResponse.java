package net.tepex.alarm.commons;

/**
 * Простой текстовый ответ сервера.
 */
public class StringResponse
{
	public StringResponse()
	{
		this("OK");
	}
	
	public StringResponse(String msg)
	{
		this.msg = msg;
	}
	
	private String msg;
}