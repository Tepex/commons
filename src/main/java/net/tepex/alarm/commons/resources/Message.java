package net.tepex.alarm.commons.resources;

import java.util.Date;
import java.util.Map;

/**
 * Реализация сообщения.
 */
public class Message extends AlarmEntity
{
	public Message(long id, long from, long to, String text, Date date, Date delivered, boolean isInbox)
	{
		super(id);
		this.from = from;
		this.to = to;
		this.text = text;
		this.date = date;
		this.delivered = delivered;
		this.isInbox = isInbox;
	}
	
	public long getFrom()
	{
		return from;
	}
	
	public long getTo()
	{
		return to;
	}
	
	public String getText()
	{
		return text;
	}
	
	public Date getDate()
	{
		return date;
	}
	
	public Date getDelivered()
	{
		return delivered;
	}
	
	public boolean isInbox()
	{
		return isInbox;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if(obj == null) return false;
		if(!(obj instanceof Message)) return false;
		Message other = (Message)obj;
		return (id == other.id &&
			from == other.from &&
			to == other.to &&
			text.equals(other.text) &&
			date.equals(other.date) &&
			delivered.equals(other.delivered));
	}
	
	private long from;
	private long to;
	private String text;
	private Date date;
	private Date delivered;
	private boolean isInbox;
}
