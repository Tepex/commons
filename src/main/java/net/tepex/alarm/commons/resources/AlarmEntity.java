package net.tepex.alarm.commons.resources;

/**
 * Абстрактная реализация интерфейса сущности.
 */
public class AlarmEntity
{
	public AlarmEntity(long id)
	{
		setId(id);
	}
	/**
	 * Возвращает идентификатор сущности.
	 * @return Идентификатор сущности.
	 */
	public long getId()
	{
		return id;
	}
	
	public void setId(long id)
	{
		this.id = id;
		hash = (int)(id % Integer.MAX_VALUE);
	}

	@Override
	public int hashCode()
	{
		return hash;
	}
	
	/** Идентификатор сущности. */
	protected long id;
	private transient int hash;
}
