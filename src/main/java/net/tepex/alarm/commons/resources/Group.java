package net.tepex.alarm.commons.resources;

/**
 * Реализация ресурса "Группы".
 */
public class Group extends AlarmEntity
{
	public Group(long id, String name)
	{
		super(id);
		this.name = name;
	}
	
	public String getName()
	{
		return name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if(obj == null) return false;
		if(!(obj instanceof Group)) return false;
		Group other = (Group)obj;
		return id == other.id &&
			name.equals(other.name);

	}
	
	@Override
	public String toString()
	{
		return name+" ["+id+"]";
	}
	
	private String name;
}