package net.tepex.alarm.commons.resources;

import java.util.Date;

/**
 * Реализация пользователя.
 */
public class User extends AlarmEntity
{
	public User(
		long id,
		String login,
		String pwd,
		String email,
		String name,
		double latitude,
		double longitude,
		Date lastAccess,
		Date created,
		String model,
		String serial,
		String platform)
	{
		super(id);
		this.login = login;
		this.password = pwd;
		this.email = email;
		this.name = name;
		this.latitude = latitude;
		this.longitude = longitude;
		this.lastAccess = lastAccess;
		this.created = created;
		this.model = model;
		this.serial = serial;
		this.platform = platform;
	}
	
	/**
	 * Возвращает логин пользователя.
	 * @return String Логин пользователя.
	 */
	public String getLogin()
	{
		return login;
	}
	
	/**
	 * Возвращает пароль пользователя.
	 * @return String Пароль пользователя.
	 */
	public String getPassword()
	{
		return password;
	}
	
	/**
	 * Возвращает Email пользователя.
	 * @return String Email пользователя.
	 */
	public String getEmail()
	{
		return email;
	}
	
	/**
	 * Возвращает имя пользователя.
	 * @return String Имя пользователя.
	 */
	public String getName()
	{
		return name;
	}
	
	/**
	 * Возвращает время последнего доступа.
	 * @return Date Время последнего доступа.
	 */
	public Date getLastAccess()
	{
		return lastAccess;
	}
	
	public double getLatitude()
	{
		return latitude;
	}
	
	public void setLatitude(double latitude)
	{
		this.latitude = latitude;
	}
	
	public double getLongitude()
	{
		return longitude;
	}
	
	public void setLongitude(double longitude)
	{
		this.longitude = longitude;
	}
	
	public String getModel()
	{
		return model;
	}
	
	public String getSerial()
	{
		return serial;
	}
	
	public String getPlatform()
	{
		return platform;
	}
	
	/**
	 * Возвращает дату создания.
	 * @return Date Дата создания.
	 */
	public Date getCreatedDate()
	{
		return created;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if(obj == null) return false;
		if(!(obj instanceof User)) return false;
		User other = (User)obj;
		return (other.id == id) &&
			other.model.equals(model) &&
			other.serial.equals(serial) &&
			other.platform.equals(platform) &&
			other.login.equals(login) &&
			other.password.equals(password) &&
			other.email.equals(email) &&
			other.name.equals(name) &&
			other.latitude == latitude &&
			other.longitude == longitude;
	}
	
	@Override
	public String toString()
	{
		return name+" ["+login+" ("+id+")] location ["+latitude+", "+longitude+"]";
	}
	
	private String model;
	private String serial;
	private String platform;
	private String login;
	private String password;
	private String email;
	private String name;
	private Date lastAccess;
	private double latitude;
	private double longitude;
	private Date created;
}