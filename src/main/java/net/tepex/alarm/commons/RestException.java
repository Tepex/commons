package net.tepex.alarm.commons;

import retrofit.RetrofitError;

/**
 * Исключение для REST-методов протокола Alarm.
 * Выпадает, если HTTP-код возврата 400, 404 или 500.
 */
public class RestException extends RuntimeException
{
	public RestException(RetrofitError cause)
	{
		super(cause);
	}
}