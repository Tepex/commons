package net.tepex.alarm.commons;

import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Query;

import net.tepex.alarm.commons.resources.User;
import net.tepex.alarm.commons.resources.Message;

/**
 * Реализация RESTful протокола обмена собщениями между сервером и клиентом.
 * Используется на клиенте. Юнит-тесты также используют эту библиотеку в качестве клиента.
 * Используется роботами. Используется сторонними ресурсами (вэб-сайты-эмуляторы).
 */
public interface RestApi
{
	/**
	 * Получение списка пользователей (друзей).
	 * Если groupId=0, то возвращаются все пользователи.
	 * Иначе возвращаются друзья пользователя {id} в группе {groupId}.
	 *
	 * @param id			 id пользователя.
	 * @param groupId	 id группы или 0 - для всех пользователей.
	 * @param isOnline	 true - только онлайн-пользователи.
	 * @param special	 Если не 0, то возвращаются только спец. пользователи с типом special.
	 * @return			 список пользователей.
	 */
	@GET(RESOURCE_USERS)
	public User[] users(
		@Query(PARAM_ID) long id,
		@Query(PARAM_GROUP_ID) long groupId,
		@Query(PARAM_ONLINE) boolean isOnline,
		@Query(PARAM_SPECIAL) int special) throws RestException;
	
	/**
	 * Авторизация пользователя.
	 *
	 * @param login				 логин пользователя.
	 * @param pwd				 пароль пользователя.
	 * @exception RestException	 выбрасывается при статус коде HTTP 404, 500
	 * @return					 в случае успеха возвращает авторизованного пользователя. {@link User}
	 */
	@GET(RESOURCE_USER_LOGIN)
	public UserAnswer login(
		@Query(PARAM_LOGIN) String login,
		@Query(PARAM_PWD) String pwd) throws RestException;
	
	/**
	 * Регистрация пользователя.
	 *
	 * @param user				 данные нового пользователя.
	 * @exception RestException	 выбрасывается при статус коде HTTP 500
	 * @return					 в случае успеха возвращает зарегистрированного пользователя.
	 */
	@POST(RESOURCE_USERS)
	public UserAnswer register(
		@Body User user) throws RestException;
	
	/**
	 * Пинг.
	 * 
	 * @param id					 id пользователя.
	 * @param groupId			 id группы.
	 * @param latitude			 широта пользователя.
	 * @param longitude			 долгота пользователя.
	 * @exception RestException	 выбрасывается при статус коде HTTP 400, 500 
	 * @return					 в случае успеха возвращает данные для пользователя. {@link PingAnswer}
	 */
	@PUT(RESOURCE_USER_PING)
	public PingAnswer ping(
		@Query(PARAM_ID) long id,
		@Query(PARAM_GROUP_ID) long groupId,
		@Query(PARAM_LATITUDE) double latitude,
		@Query(PARAM_LONGITUDE) double longitude) throws RestException;
	
	
	/*---------- работа с чатом -----------*/
	
	/**
	 * Получение чата пользователя.
	 *
	 * @param id					 id пользователя.
	 * @param friendId			 id друга пользователя.
	 * @exception RestException	 выбрасывается при статус коде HTTP 400, 500
	 * @return					 список сообщений чата
	 */
	@GET(RESOURCE_CHAT)
	public Message[] chat(
		@Query(PARAM_ID) long id, 
		@Query(PARAM_FRIEND_ID) long friendId) throws RestException;
	
	/**
	 * Удаление сообщений чата.
	 *
	 * @param id					 список id сообщений для удаления.
	 * @exception RestException	 выбрасывается при статус коде HTTP 400, 500
	 * @return					 текстовое сообщение
	 */
	@DELETE(RESOURCE_CHAT)
	public StringResponse deleteMessages(
		@Query(PARAM_ID) long ... id) throws RestException;
	
	/**
	 * Создание сообщения.
	 *
	 * @param id					 id пользователя.
	 * @param friendId			 id получателя.
	 * @exception RestException	 выбрасывается при статус коде HTTP 400, 500
	 * @return					 новое сообщение.
	 * 
	 */
	@POST(RESOURCE_CHAT)
	public Message newMessage(
		@Query(PARAM_ID) long id,
		@Query(PARAM_FRIEND_ID) long friendId) throws RestException;
	
	/* ----------- Работа с друзьями ------------ */
	
	/**
	 * Добавление в друзья.
	 * 
	 * @param id					 id пользователя.
	 * @param friendId			 id друга.
	 * @param groupId			 id группы.
	 * @return					 добавленный друг.
	 * @exception RestException	 выбрасывается при статус коде HTTP 400, 500
	 */
	@POST(RESOURCE_FRIENDS)
	public User addFriend(
		@Query(PARAM_ID) long id,
		@Query(PARAM_FRIEND_ID) long friendId,
		@Query(PARAM_GROUP_ID) long groupId) throws RestException;
	
	/**
	 * Удаление из друзей.
	 *
	 * @param id					 id пользователя.
	 * @param friendsId			 список id друзей пользователя.
	 * @exception RestException	 выбрасывается при статус коде HTTP 400, 500
	 * @return					 текстовое сообщение
	 */
	@DELETE(RESOURCE_FRIENDS)
	public StringResponse deleteFriends(
		@Query(PARAM_ID) long id,
		@Query(PARAM_FRIEND_ID) long ... friendsId) throws RestException;
	
	public static final String RESOURCE_USERS = "/users";
	public static final String RESOURCE_USER = "/user";
	public static final String RESOURCE_USER_LOGIN = RESOURCE_USER+"/login";
	public static final String RESOURCE_USER_PING = RESOURCE_USER+"/ping";
	public static final String RESOURCE_CHAT = "/chat";
	public static final String RESOURCE_FRIENDS = "/friends";
	
	public static final String PARAM_ID = "id";
	public static final String PARAM_LOGIN = "login";
	public static final String PARAM_PWD = "pwd";
	public static final String PARAM_LATITUDE = "lt";
	public static final String PARAM_LONGITUDE = "ln";
	
	public static final String PARAM_LIST_TYPE = "type";
	
	public static final String PARAM_FRIEND_ID = "friendId";
	public static final String PARAM_GROUP_ID = "groupId";
	
	public static final String PARAM_ONLINE = "online";
	public static final String PARAM_SPECIAL = "special";
}
