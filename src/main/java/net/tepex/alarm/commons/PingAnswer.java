package net.tepex.alarm.commons;

import net.tepex.alarm.commons.resources.User;

/**
 * Ответ сервера на пинг. Возвращает рекомендуемый период пинга в секундах и
 * список пользователей для отображения на карте.
 */
public class PingAnswer
{
	public PingAnswer(int ping, User[] users)
	{
		this.ping = ping;
		this.users = users;
	}
	
	public int getPing()
	{
		return ping;
	}
	
	public User[] getUsers()
	{
		return users;
	}
	
	private int ping;
	private User[] users;
}
